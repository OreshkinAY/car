import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatTabsModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatTableModule,
  MatDialogModule
} from '@angular/material';
import { HeaderComponent } from './components/header/header.component';
import { InspectionNextTabComponent } from './components/button/inspection-next-tab/inspection-next-tab.component';
import { PhotoLoadComponent } from './components/photo/photo-load/photo-load.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    RouterModule,
    MatTableModule,
    MatDialogModule
  ],
  providers: [],
  declarations: [
    HeaderComponent,
    InspectionNextTabComponent,
    PhotoLoadComponent,
  ],
  exports: [
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTabsModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    HeaderComponent,
    InspectionNextTabComponent,
    PhotoLoadComponent,
    MatTableModule,
    MatDialogModule
  ]
})
export class SharedModule {
}
