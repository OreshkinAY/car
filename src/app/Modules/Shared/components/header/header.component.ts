import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../../../Auth/Services/auth.service';
import headerLabel from './../../const/header';


@Component({
  selector: 'cc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  navLinks: any[];
  constructor(private router: Router, private authService: AuthService) {
    this.navLinks = headerLabel;
  }

  onExit() {
      this.authService.clearToken();
      this.router.navigate(['/auth']);
  }
}
