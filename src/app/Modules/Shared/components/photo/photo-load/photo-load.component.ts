import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { map, mergeMap, concatMap } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

import { PhotoVideoService } from 'src/app/Modules/Main/Services/inspection/photo-video.service';
import { InspectionPhotoVideo } from 'src/app/Modules/Main/Models/Inspection-photo-video';

@Component({
  selector: 'cc-photo-load',
  templateUrl: './photo-load.component.html',
  styleUrls: ['./photo-load.component.scss']
})
export class PhotoLoadComponent implements OnInit {
  idImage: number;
  imageData: object;
  @Output() imageLoaded = new EventEmitter<any>();
  @Output() spinerShow = new EventEmitter<any>();

  @Input() fieldsData: string;
  @Input() resultView: boolean;
  @Input() typeFile: string;

  @ViewChild('loadFile')
  loadFileVariable: ElementRef;

  constructor(
    private photoVideoService: PhotoVideoService,
    private domSanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
  }

  fileLoad(event) {
    if (event.target.files.length > 0) {
      const fileList: FileList = event.target.files;
      const file: File = fileList[0];
      this.saveFile(file);
    }
  }

  acceptFile() {
    const acceptImg = '.jpeg, .png, image/png, image/jpeg';
    const acceptVideo = '.mp4, video/mp4';
    return this.typeFile === 'Video' ? acceptVideo : acceptImg;
  }

  saveFile(file) {
    this.spinerShow.emit({ [this.fieldsData]: true });

    this.photoVideoService.getReservedFileId({ 'count': 1 }).pipe(
      concatMap((response: any) => {
        const formData: FormData = new FormData();
        formData.append('name', 'files');
        formData.append('files', file, response[0]);
        formData.append('Content-Type', file.type);
        return this.photoVideoService.sendFileContent(formData);
      }),
      concatMap((resp: any) => {
        this.idImage = resp[0];
        return this.photoVideoService.getThumbnail(resp[0]);
      })
    ).subscribe(
      (dataImage) => {
        this.imageData = this.createUrlImage(dataImage);
        const img = new InspectionPhotoVideo(this.idImage, this.imageData);
        this.imageLoaded.emit({ img, fieldsData: this.fieldsData });
        // this.spinerShow.emit({ [this.fieldsData]: false });
        this.loadFileVariable.nativeElement.value = '';
      },
      (error) => error
    );
  }

  createUrlImage(dataImage) {
    return this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(dataImage));
  }

}
