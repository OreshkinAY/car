import { Component, OnInit, Input } from '@angular/core';

import { InspectionService } from 'src/app/Modules/Main/Services/inspection/inspection.service';

@Component({
  selector: 'cc-inspection-next-tab',
  templateUrl: './inspection-next-tab.component.html',
  styleUrls: ['./inspection-next-tab.component.scss']
})
export class InspectionNextTabComponent implements OnInit {
  @Input() currentIndexTab: number;

  constructor(private inspectionService: InspectionService) { }

  ngOnInit() {
  }

  nextTab() {
    this.inspectionService.changeNextTab(this.currentIndexTab);
  }

}
