const headerLabel = [
    {
        label: 'Изъятие',
        link: 'withdrawal',
    },
    {
        label: 'Контроль',
        link: 'control',
    },
    {
        label: 'Рекламация',
        link: 'complaints',
    },
    {
        label: 'Отчеты',
        link: 'reports',
    }
];

export default headerLabel;
