const inspectionsTabs = [
    {
        label: 'Информация о ТС',
        type: 'info',
    },
    {
        label: 'Фото / Видео',
        type: 'photo',
    },
    {
        label: 'Повреждения',
        type: 'damage',
    },
    {
        label: 'Результат осмотра',
        type: 'result-inspection',
    },
    {
        label: 'Завершение',
        type: 'complete',
    }

];

export default inspectionsTabs;
