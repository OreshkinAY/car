import { Injectable } from '@angular/core';
import { HttpApiService } from './../../Core/Services/http-api.service';
import { Observable } from 'rxjs';
import { Contracts } from './../Models/Abstract/ContractsData.interface';

@Injectable()
export class ContractsService {
    constructor(private httpApi: HttpApiService) { }

    public getContracts(): Observable<Contracts[]> {
        return this.httpApi.get('/contracts');
    }
}
