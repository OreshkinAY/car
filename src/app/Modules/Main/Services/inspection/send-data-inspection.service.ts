import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { map, switchMap, takeUntil, catchError } from 'rxjs/operators';

import { HttpApiService } from './../../../Core/Services/http-api.service';
import { InspectionParamsId } from 'src/app/Modules/Main/Models/inspection-params-id';
import { CompleteService } from 'src/app/Modules/Main/Services/inspection/complete.service';
import { InspectionService } from 'src/app/Modules/Main/Services/inspection/inspection.service';
import { DamagesService } from 'src/app/Modules/Main/Services/inspection/damages.service';
import { DamagesNameId } from 'src/app/Modules/Main/Models/const/damages-name';

@Injectable()
export class SendDataInspectionService implements OnDestroy {
    private validComponentDamages: boolean;
    private ngUnsubscribe: Subject<void> = new Subject<void>();
    allData = {};
    valuesFormFields: object = {};
    parkingId: string;

    constructor(
        private completeService: CompleteService,
        private inspectionService: InspectionService,
        private damagesService: DamagesService,
        private httpApi: HttpApiService
    ) {
    }

    takeDataTabsInspection() {
        this.dataTabDamages();
        this.dataTabInfoCar();
    }

    sendData() {
        return this.dataTabPhotoVideo().pipe(
            switchMap(_ => {
                return this.assemblyData();
            }),
            switchMap(parking => {
                return this.getReservedFileId([parking]);
            }),
            catchError(error => {
                return throwError(error.msg);
            })
        );
    }

    dataTabPhotoVideo() {
        return this.inspectionService.photos$.pipe(
            takeUntil(this.ngUnsubscribe)
        ).pipe(map(photos => {
            const generalPhotos = [];
            // поле видео обязательное
            if (photos.hasOwnProperty('VIDEO_360')) {
                Object.keys(photos).forEach(
                    key => {
                        generalPhotos.push({ viewShortName: key, fileId: photos[key].id });
                    }
                );
                this.allData['generalPhotos'] = generalPhotos;
                return this.allData;
            } else {
                console.error('поле видео обязательное');
                throw ({ 'msg': 'generalVideoEmpty' });
            }
        }));

    }

    assemblyData() {
        this.allData['carId'] = this.inspectionService.idCar;
        this.allData['datetime'] = this.calcTime();
        this.allData['typeShortName'] = 'TRANSFER';
        this.allData['followUpShortName'] = 'CAR_WITHDRAWAL';
        this.allData['address'] = 'test';

        if (!this.parkingId) {
            return throwError({ 'msg': 'parkingIdEmpty' });
        } else {
            this.allData['parkingId'] = this.parkingId;
        }

        this.getPhoto();
        this.getVideo();
        this.takeDataTabsInspection();

        return Observable.create(observer => observer.next(this.allData));
    }

    getReservedFileId(data): Observable<number[]> {
        return this.httpApi.post('/inspection', data);
    }

    getPhoto() {
        this.completeService.photoDamadeComplete$.subscribe(photo => {
            this.allData['signatureFileId'] = !photo.length ? '' : photo[0].id;
        });
    }

    getVideo() {
        this.completeService.videoDamadeComplete$.subscribe(video => {
            this.allData['videoFileId'] = !video.length ? '' : video[0].id;
        });
    }

    calcTime() {
        const d = new Date();
        const utc = d.getTime() - (d.getTimezoneOffset() * 60000);
        return new Date(utc).toISOString().split('.')[0];
    }

    dataTabInfoCar() {
        this.inspectionService.inspectionFields$.pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(valuesFormFields => {
            this.valuesFormFields = valuesFormFields;
        });

        this.inspectionService.infoForm$.pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(valuesFormFields => {
            if (valuesFormFields && valuesFormFields.valid) {
                const initialInspection = [];
                Object.keys(valuesFormFields.value).forEach(idField => {
                    if (InspectionParamsId[idField]) {
                        if (this.valuesFormFields[idField].parameter.type === 'input') {
                            initialInspection.push({
                                parameter: InspectionParamsId[idField],
                                chosenValue: null,
                                comment: valuesFormFields.value[idField].chosenValue
                            });
                        } else {
                            initialInspection.push({
                                parameter: InspectionParamsId[idField],
                                chosenValue: valuesFormFields.value[idField].chosenValue,
                                comment: this.valuesFormFields[idField].comment === undefined ? '' : this.valuesFormFields[idField].comment
                            });
                        }
                    }
                });
                this.allData['initialInspection'] = initialInspection;
            }
        });
    }

    dataTabDamages() {
        this.damagesService.components$.pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(components => {
            function isValidDamages(componentDamages) {
                return componentDamages.validate;
            }
            // все повреждения подтверждены
            this.validComponentDamages = components.every(isValidDamages);

            const damagedComponents = [];
            components.forEach(component => {
                if (component.validate) {
                    // только damage: true
                    const damages = Object.keys(component.damages)
                        .filter(damageSelect => component.damages[damageSelect])
                        .map(idDamageSelect => `${DamagesNameId[idDamageSelect]}`);
                    if (damages.length) {
                        const photos = component.photo.map(photoLoad => {
                            return photoLoad.id;
                        });

                        const damagesComponent = {
                            comment: component.comment,
                            component: component.idComponent,
                            damages,
                            photos
                        };
                        damagedComponents.push(damagesComponent);
                    }

                    if (component.field === 'General_condition' && component.comment) {
                        this.allData['comment'] = component.comment;
                    }
                }
            });

            if (damagedComponents.length) {
                this.allData['damagedComponents'] = damagedComponents;
            }
        });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
