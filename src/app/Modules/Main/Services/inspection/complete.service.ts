import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { HttpApiService } from './../../../Core/Services/http-api.service';

@Injectable()
export class CompleteService {
    private _videoDamadeComplete = new BehaviorSubject<any>([]);
    public videoDamadeComplete$ = this._videoDamadeComplete.asObservable();

    private photoDamadeComplete = new BehaviorSubject<any>([]);
    public photoDamadeComplete$ = this.photoDamadeComplete.asObservable();

    constructor(private httpApi: HttpApiService) { }

    public getParking(): Observable<any[]> {
        return this.httpApi.get('/parkings/by_user');
    }

    public saveMedia(data) {
        if (data.fieldsData === 'photo') {
            data.img.type = 'photo';
            this.photoDamadeComplete.next(this.photoDamadeComplete.getValue().concat(data.img));
            return;
        }
        this._videoDamadeComplete.next(this._videoDamadeComplete.getValue().concat(data.img));
        return;
    }

    public removeMedia(field) {
        if (field === 'photo') {
            this.photoDamadeComplete.next([]);
            return;
        }
        this._videoDamadeComplete.next([]);
    }
}
