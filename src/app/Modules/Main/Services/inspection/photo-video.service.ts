import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { HttpApiService } from './../../../Core/Services/http-api.service';

@Injectable()
export class PhotoVideoService {

    constructor(
        private httpApi: HttpApiService,
        private domSanitizer: DomSanitizer, ) { }

    public createDataCarousel(components): Observable<any> {
        return components.map((process) => {
            return this.getBigSize(process.id).pipe(
                map(tasks => {
                    process.bigFile = this.createUrlImage(tasks);
                    return tasks;
                }),
                catchError((error: any) => {
                    console.error('Error loading image original size:' + error);
                    return throwError(error.statusText);
                })
            );
        });
    }

    public createUrlImage(dataImage) {
        return this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(dataImage));
    }

    public getReservedFileId(data): Observable<number[]> {
        return this.httpApi.post('/files/create', data);
    }

    public sendFileContent(data): Observable<number[]> {
        return this.httpApi.put('/files', data);
    }

    public getThumbnail(idPhoto) {
        return this.httpApi.getBlob(`/files/${idPhoto}/thumbnail`);
    }

    public getBigSize(idFile) {
        return this.httpApi.getBlob('/files/' + idFile);
    }
}
