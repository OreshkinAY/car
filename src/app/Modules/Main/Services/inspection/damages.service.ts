import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { HttpApiService } from '../../../Core/Services/http-api.service';
import ComponentDamage from '../../Models/component-damage';
import { DamagesDetailsId } from '../../Models/const/damages-details';
import { DamagesComponent } from './../../Models/Abstract/DamagesComponent.interface';
@Injectable()
export class DamagesService {
    private _damages: BehaviorSubject<any> = new BehaviorSubject([]);
    private _components: BehaviorSubject<any> = new BehaviorSubject([]);
    componentsList: DamagesComponent[] = [];
    damages$ = this._damages.asObservable();
    components$ = this._components.asObservable();

    constructor(
        private httpApi: HttpApiService,
    ) {
        this.loadDamages();
    }

    public loadDamages() {
        this.httpApi.get('/inspection/script').subscribe(
            (response) => {
                this._damages.next(response.damages);
                response.components.forEach(component => {
                    const componentDamage = new ComponentDamage(
                        component.id,
                        DamagesDetailsId[component.id],
                        component.name,
                        component.possibleDamages
                    );
                    this.componentsList.push(componentDamage);
                });
                const lastId = +this.componentsList[this.componentsList.length - 1].idComponent + 1;
                const componentGeneralСondition = new ComponentDamage(
                    lastId.toString(),
                    'General_condition',
                    'Общее состояние автомобиля'
                );
                this.componentsList.push(componentGeneralСondition);
                this._components.next(this.componentsList);
            },
            (error) => {
                this._damages.error(error);
                this._components.error(error);
            }
        );
    }

    public validateCheckboxAndPhoto(components) {
        const selectedDamageCount = Object.keys(components.damages).filter(
            controlName => {
                return components.damages[controlName] === true;
            }
        ).length;

        switch (true) {
            case components.photo.length && !selectedDamageCount:
                return true;
            case !components.photo.length && !!selectedDamageCount:
                return true;
            case components.photo.length < selectedDamageCount:
                return true;
            default:
                return false;
        }
    }
}
