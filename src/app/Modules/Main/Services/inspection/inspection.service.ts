import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { FormGroup } from '@angular/forms';

import { Brand } from './../../Models/Abstract/Brand.interface';
import { ClientСontract } from './../../Models/Abstract/CarClient.interface';
import { ModelCar } from './../../Models/Abstract/ModelCar.interface';
import { HttpApiService } from './../../../Core/Services/http-api.service';
import { InspectionParameter } from './../../Models/inspectionParameter';
import { InspectionParameters } from './../../Models/Abstract/InspectionParameters.interface';
import { CarClient } from './../../Models/Abstract/CarClient.interface';

@Injectable()
export class InspectionService {
    public inspectionFields: object = {};
    public inspectionPhotoVideo: object = {};
    public inspectionParameters: InspectionParameters[];
    public inspectionCar: CarClient;
    public idCar: string;

    private _selectedPageIndex: BehaviorSubject<any> = new BehaviorSubject(0);
    public selectedPageIndex$ = this._selectedPageIndex.asObservable();

    private _photos: BehaviorSubject<any> = new BehaviorSubject(this.inspectionPhotoVideo);
    public photos$ = this._photos.asObservable();

    private _infoForm = new BehaviorSubject<FormGroup>(null);
    public infoForm$ = this._infoForm.asObservable();

    private _inspectionFields: BehaviorSubject<any> = new BehaviorSubject(this.inspectionFields);
    public inspectionFields$ = this._inspectionFields.asObservable();

    constructor(private httpApi: HttpApiService) { }

    public changeNextTab(pageNumber) {
        this._selectedPageIndex.next(pageNumber + 1);
    }

    public imageLoaded(data) {
        this.inspectionPhotoVideo[data.fieldsData] = data.img;
        this._photos.next(this.inspectionPhotoVideo);
    }

    public newInspectionInfo(field, parameter) {
        this.inspectionFields[field] = new InspectionParameter(parameter);
    }

    public newInspectionCarInfo(field) {
        this.inspectionFields[field] = {};
    }

    public saveInspectionInfo(field, chosenValue, comment?) {
        this.inspectionFields[field]['chosenValue'] = chosenValue;
        if (comment !== undefined) {
            this.inspectionFields[field]['comment'] = comment;
        }
        this._inspectionFields.next(this.inspectionFields);
    }

    public getInspectionInfo(controlName) {
        return this.inspectionFields[controlName];
    }

    public getInspection() {
        return this.inspectionFields;
    }

    public getFieldValue(controlName) {
        if (this.inspectionFields[controlName]) {
            return this.inspectionFields[controlName];
        }
        return null;
    }

    public getInspectionParameters(): Observable<InspectionParameters[]> {
        return this.httpApi.get('/inspection/parameters');
    }

    public getBrands(): Observable<Brand[]> {
        return this.httpApi.get('/make/all');
    }

    public getBrandsContract(make, brands) {
        return brands.filter(el => {
            return el.name.toLowerCase().indexOf(make.toLowerCase()) > -1;
        });
    }

    public getInspectionCar(idCar: string): Observable<ClientСontract> {
        this.idCar = idCar;
        return this.httpApi.get('/contracts/by_car/' + idCar);
    }

    public getModel(idMake): Observable<ModelCar[]> {
        return this.httpApi.get('/make/models_by_make/' + idMake);
    }

    public saveFormInfo(form: FormGroup) {
        this._infoForm.next(form);
    }

}
