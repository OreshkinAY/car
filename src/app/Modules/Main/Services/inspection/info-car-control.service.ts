import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { InspectionParamsId } from '../../Models/inspection-params-id';
import СommentsFieldsInspection from './../../../Main/Models/const/comments-fields-inspection';
import { InspectionService } from './inspection.service';
import { InspectionParameters } from './../../Models/Abstract/InspectionParameters.interface';
import { CarClient } from 'src/app/Modules/Main/Models/Abstract/CarClient.interface';

@Injectable()
export class InfoCarControlService {
    public fieldsCtrls: any = {};

    constructor(private inspectionService: InspectionService) { }

    toFormGroup(inspectionParameters: InspectionParameters[], component: string, inspectionCar?: CarClient) {
        this.fieldsCtrls[component] = {};
        inspectionParameters.forEach((parameter: any) => {
            const idField = parameter.id;
            if (InspectionParamsId[idField]) {

                const field = InspectionParamsId[idField];
                const fieldParams = this.inspectionService.getInspectionInfo(field);

                if (!fieldParams) {
                    if (parameter.values.length > 0) {
                        parameter.type = 'select';
                        this.inspectionService.newInspectionInfo(field, parameter);
                    } else {
                        parameter.type = 'input';
                        this.inspectionService.newInspectionInfo(field, parameter);
                    }
                }
                const valueControl = this.inspectionService.getFieldValue(field);
                if (СommentsFieldsInspection[idField]) {
                    this.fieldsCtrls[component][field] = new FormGroup({
                        'chosenValue': new FormControl(valueControl.chosenValue, [Validators.required]),
                        'comment': new FormControl(valueControl.comment),
                    });
                } else {
                    this.fieldsCtrls[component][field] = new FormGroup({
                        'chosenValue': new FormControl(valueControl.chosenValue, [Validators.required]),
                    });
                    // this.fieldsCtrls[component][field] = new FormControl(valueControl.chosenValue, [Validators.required]);
                }
            }

        });

        if (inspectionCar) {
            this.createFieldCorporateDate(inspectionCar, component);
            return new FormGroup(this.fieldsCtrls[component], { updateOn: 'blur' });
        }
        return new FormGroup(this.fieldsCtrls[component], { updateOn: 'blur' });
    }

    createFieldCorporateDate(carData, component) {
        const infoCar = {
            'BRAND': carData.make,
            'MODEL': carData.model,
            'YEAR_ISSUE': carData.manufactureYear,
            'HORSEPOWER': carData.enginePower,
            'COLOR': carData.color,
            'GOS_NUMBER': carData.licensePlate,
            'VIN': carData.vin
        };

        Object.keys(infoCar).forEach(field => {
            const fieldParams = this.inspectionService.getInspectionInfo(field);

            if (!fieldParams) {
                this.inspectionService.newInspectionCarInfo(field);
                this.inspectionService.saveInspectionInfo(field, infoCar[field]);
            }

            this.fieldsCtrls[component][field] =
                new FormControl(this.inspectionService.getFieldValue(field).chosenValue, [Validators.required]);
        });
    }
}
