import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PagesRoutes } from './pages.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './../Core/core.module';

import { AuthPageComponent } from './../Auth/Components/auth-page/auth-page.component';
import { SharedModule } from './../Shared/shared.module';
import { PagesRootComponent } from './Components/pages-root/pages-root.component';
import { ControlPageComponent } from './../Main/Components/control-page/control-page.component';
import { ComplaintsPageComponent } from './../Main/Components/complaints-page/complaints-page.component';
import { ReportsPageComponent } from './../Main/Components/reports-page/reports-page.component';
import { WithdrawalPageComponent } from './../Main/Components/withdrawal-page/withdrawal-page.component';
import { LeftPanelComponent } from './Components/withdrawal-page/left-panel/left-panel.component';
import { RightPanelComponent } from './Components/withdrawal-page/right-panel/right-panel.component';
import { ContractsService } from './../Main/Services/contracts.service';
import { InspectionService } from './Services/inspection/inspection.service';
import { PhotoVideoService } from './Services/inspection/photo-video.service';
import { DamagesService } from './Services/inspection/damages.service';
import { InfoCarControlService } from './Services/inspection/info-car-control.service';
import { SearchContractPipe } from './../Main/Components/withdrawal-page/left-panel/search-contract.pipe';
import { InspectionComponent } from './Components/inspection/inspection.component';
import { InfoComponent } from './Components/inspection/info/info.component';
import { PhotoComponent } from './Components/inspection/photo/photo.component';
import { ResultInspectionComponent } from './Components/inspection/result-inspection/result-inspection.component';
import { CorporateDataComponent } from './Components/inspection/info/corporate-data/corporate-data.component';
import { ViewFieldsComponent } from './Components/inspection/info/view-fields/view-fields.component';
import { FormBlockComponent } from './Components/inspection/info/form-block/form-block.component';
import { EnumToArrayPipe } from './Pipe/enum-to-array.pipe';
import { DamagesResultImages } from './Pipe/damages-result-images.pipe';
import { CarouselComponent } from './Components/inspection/carousel/carousel.component';
import { DamageCommentsComponent } from './Components/inspection/damage/damage-comments/damage-comments.component';
import { DamageDetailsComponent } from './Components/inspection/damage/damage-details/damage-details.component';
import { DamageResultComponent } from './Components/inspection/damage/damage-result/damage-result.component';
import { MainPhotoComponent } from './Components/inspection/photo/main/main-photo/main-photo.component';
import { CommentComponent } from './Components/inspection/damage/damage-comments/comment/comment.component';
import { CompleteRightComponent } from './Components/inspection/complete/complete-right/complete-right.component';
import { CompleteLeftComponent } from './Components/inspection/complete/complete-left/complete-left.component';
import { WithdrawKeyComponent } from './Components/inspection/complete/complete-right/withdraw-key/withdraw-key.component';
import { CompleteService } from './Services/inspection/complete.service';
import { SendParkingComponent } from './Components/inspection/complete/complete-right/send-parking/send-parking.component';
import { SendDataInspectionService } from './Services/inspection/send-data-inspection.service';
import { ErrorMessageComponent } from './Components/inspection/complete/complete-right/send-parking/send-parking.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes),
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    SharedModule
  ],
  entryComponents: [ ErrorMessageComponent],
  declarations: [
    AuthPageComponent,
    PagesRootComponent,
    ControlPageComponent,
    ComplaintsPageComponent,
    ReportsPageComponent,
    WithdrawalPageComponent,
    LeftPanelComponent,
    RightPanelComponent,
    SearchContractPipe,
    DamagesResultImages,
    EnumToArrayPipe,
    InspectionComponent,
    InfoComponent,
    PhotoComponent,
    ResultInspectionComponent,
    CorporateDataComponent,
    ViewFieldsComponent,
    FormBlockComponent,
    CarouselComponent,
    DamageCommentsComponent,
    DamageDetailsComponent,
    DamageResultComponent,
    MainPhotoComponent,
    CommentComponent,
    CompleteRightComponent,
    CompleteLeftComponent,
    WithdrawKeyComponent,
    SendParkingComponent,
    ErrorMessageComponent
  ],
  providers: [
    ContractsService,
    InspectionService,
    CompleteService,
    DamagesService,
    PhotoVideoService,
    InfoCarControlService,
    SendDataInspectionService
  ]
})
export class PagesModule { }
