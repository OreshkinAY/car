import { Component, OnInit, Input } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs';

import { GeneralView, DetailFoto } from '../../../../../Models/const/inspection-photo-video';
import { InspectionService } from './../../../../../Services/inspection/inspection.service';
import { PhotoVideoService } from '../../../../../Services/inspection/photo-video.service';
import { ImageSlider } from 'src/app/Modules/Main/Models/Abstract/ImageSlider.interface';

@Component({
  selector: 'cc-main-photo',
  templateUrl: './main-photo.component.html',
  styleUrls: ['./main-photo.component.scss']
})
export class MainPhotoComponent implements OnInit {
  @Input() resultView: boolean;
  generalView = GeneralView;
  detailFoto = DetailFoto;
  showCarousel = false;
  imageData: any;
  imageLoading: object = {};
  fieldsData: object;
  images: ImageSlider[] = [];
  idImageBig: number;
  errorLoadImg: string;
  errorSaveImg: string;

  constructor(
    private inspectionService: InspectionService,
    private photoVideoService: PhotoVideoService,
  ) { }

  ngOnInit() {
    this.fieldsData = this.inspectionService.inspectionPhotoVideo;
  }

  spinerShow(data) {
    this.imageLoading = data;
  }

  showOriginalSize(idImg) {
    this.processHeader('0');
    document.body.style.overflow = 'hidden';
    this.images = [];

    const componentsData = [];
    Object.keys(this.inspectionService.inspectionPhotoVideo).forEach(data => {
      componentsData.push(this.inspectionService.inspectionPhotoVideo[data]);
    });

    const tasksObservables: Observable<any> = this.photoVideoService.createDataCarousel(componentsData);
    forkJoin(tasksObservables).subscribe(
      () => {
        Object.keys(this.inspectionService.inspectionPhotoVideo).forEach((process) => {
          const type = process === 'VIDEO_360' ? 'video' : 'photo';
          this.images.push({
            'id': this.inspectionService.inspectionPhotoVideo[process].id,
            'bigFile': this.inspectionService.inspectionPhotoVideo[process].bigFile,
            'type': type
          });
        });

        this.idImageBig = idImg;
        this.showCarousel = true;
      },
      (error) => this.errorLoadImg = error
    );
  }

  processHeader(value) {
    document.getElementById('header').style.zIndex = value;
  }

  closeCarousel() {
    this.processHeader('2');
    document.body.style.overflow = 'visible';
    this.showCarousel = false;
  }

  removeMiniature(field) {
    delete this.inspectionService.inspectionPhotoVideo[field];
  }
}
