import { Component, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { Observable, forkJoin, concat, zip, combineLatest } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { CompleteItems } from 'src/app/Modules/Main/Models/const/comlete-items';
import { CompleteService } from 'src/app/Modules/Main/Services/inspection/complete.service';
import { PhotoVideoService } from 'src/app/Modules/Main/Services/inspection/photo-video.service';
import { ImageSlider } from 'src/app/Modules/Main/Models/Abstract/ImageSlider.interface';
@Component({
  selector: 'cc-complete-left',
  templateUrl: './complete-left.component.html',
  styleUrls: ['./complete-left.component.scss']
})
export class CompleteLeftComponent implements OnInit {
  completeItems = CompleteItems;
  activeItem;
  @Output() showCompleteRight = new EventEmitter<any>();
  dataVideoComplete: any;
  dataPhotoComplete: any;
  images: ImageSlider[] = [];
  idImageBig: number;
  errorLoadImg: string;
  showCarousel = false;
  imageLoading: any = <any>{};
  @Output() closeDamageComments = new EventEmitter();

  constructor(
    private completeService: CompleteService,
    private photoVideoService: PhotoVideoService
  ) { }

  ngOnInit() {
    this.completeService.photoDamadeComplete$.subscribe((dataPhoto) => {
      this.dataPhotoComplete = dataPhoto;
    });
    this.completeService.videoDamadeComplete$.subscribe((dataVideo) => {
      this.dataVideoComplete = dataVideo;
    });
  }

  public selectItems(completeItem) {
    this.activeItem = completeItem;
    this.showCompleteRight.emit({
      'tabType': 'complete', 'component': completeItem
    });
  }

  public isActiveComponent(i) {
    return i === this.activeItem;
  }

  public imageLoaded(data) {
    this.imageLoading[data.fieldsData] = true;
    this.completeService.saveMedia(data);
  }

  spinerShow(data) {
    const key = Object.keys(data);
    this.imageLoading[key[0]] = data[key[0]];
  }

  showOriginalSize(idImg) {
    this.images = [];
    const allMedia = [...this.dataVideoComplete, ...this.dataPhotoComplete];
    const tasksObservables: Observable<any> = this.photoVideoService.createDataCarousel(allMedia);

    forkJoin(tasksObservables).subscribe(
      () => {
        allMedia.forEach((process) => {
          this.images.push({
            'id': process.id,
            'bigFile': process.bigFile,
            'type': process.type
          });

          this.processHeader('0');
          document.body.style.overflow = 'hidden';
          this.idImageBig = idImg;
          this.showCarousel = true;
        });
      },
      (error) => {
        this.errorLoadImg = error;
      }
    );
  }

  closeCarousel() {
    this.processHeader('2');
    document.body.style.overflow = 'visible';
    this.showCarousel = false;
  }

  processHeader(value) {
    document.getElementById('header').style.zIndex = value;
  }

  removeMiniature(field) {
    this.completeService.removeMedia(field);
  }

}
