import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cc-complete-right',
  templateUrl: './complete-right.component.html',
  styleUrls: ['./complete-right.component.scss']
})
export class CompleteRightComponent {
  @Input() completeItem: any;
  @Output() closeDamageComments = new EventEmitter();

  constructor() { }

  closeRightPanel() {
    this.closeDamageComments.emit();
  }
}
