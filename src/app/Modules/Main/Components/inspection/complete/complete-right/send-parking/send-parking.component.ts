import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Inject, OnDestroy } from '@angular/core';
import { map, flatMap, concatMap, switchMap, toArray, takeUntil, tap } from 'rxjs/operators';
import { Subject, Observable, timer } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

import { CompleteService } from 'src/app/Modules/Main/Services/inspection/complete.service';
import { InspectionService } from 'src/app/Modules/Main/Services/inspection/inspection.service';
import { DamagesService } from 'src/app/Modules/Main/Services/inspection/damages.service';
import { DamagesNameId } from 'src/app/Modules/Main/Models/const/damages-name';
import { SendDataInspectionService } from 'src/app/Modules/Main/Services/inspection/send-data-inspection.service';

@Component({
  selector: 'cc-send-parking',
  templateUrl: './send-parking.component.html',
  styleUrls: ['./send-parking.component.scss']
})
export class SendParkingComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['type', 'name', 'address', 'carCount'];
  dataSource;
  selectedRowIndex = -1;
  @Output() closeRightPanel = new EventEmitter();
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  infoForm: FormGroup;
  validComponentDamages: boolean;
  parkingId: string;

  constructor(
    private completeService: CompleteService,
    private inspectionService: InspectionService,
    private damagesService: DamagesService,
    private sendDataInspectionService: SendDataInspectionService,
    public error: MatDialog
  ) {
  }

  ngOnInit() {
    this.completeService.getParking().pipe(
      switchMap(parking => {
        return parking;
      }),
      switchMap(parking => {
        return parking.parkings;
      }),
      toArray()
    ).subscribe(
      parking => {
        this.dataSource = parking;
      }
    );
  }

  sendParking() {
    this.sendDataInspectionService.sendData().subscribe(
      idResultInspection => idResultInspection,
      error => {
        switch (error) {
          case 'parkingIdEmpty': {
            this.openErrorMessage(error);
            break;
          }
          case 'generalVideoEmpty': {
            this.openErrorMessage(error);
            this.inspectionService.changeNextTab(0);
            break;
          }
        }
      }
    );
  }

  highlight(row) {
    this.sendDataInspectionService.parkingId = row.id;
    this.selectedRowIndex = row.id;
  }

  isActiveRow(rowId) {
    return rowId === this.selectedRowIndex;
  }

  closePanel() {
    this.closeRightPanel.emit();
  }

  openErrorMessage(errorType: string) {
    this.error.open(ErrorMessageComponent, {
      data: {
        errorType: `${errorType}`
      }
    });
  }

  ngOnDestroy() {
    this.sendDataInspectionService.parkingId = null;
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}

@Component({
  selector: 'cc-error-message',
  templateUrl: 'error-message.html',
})
export class ErrorMessageComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data) { }
}
