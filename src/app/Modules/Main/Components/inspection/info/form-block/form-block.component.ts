import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cc-form-block',
  templateUrl: './form-block.component.html',
  styleUrls: ['./form-block.component.scss']
})
export class FormBlockComponent implements OnInit {
  @Input() title;
  @Input() fields;

  constructor() { }

  ngOnInit() { }

}
