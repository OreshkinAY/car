import { Component, OnInit, ViewEncapsulation, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { InspectionService } from './../../../Services/inspection/inspection.service';
import Fields from './../../../Models/const/fields';
import { ModelCar } from './../../../Models/Abstract/ModelCar.interface';
import { InfoCarControlService } from '../../../Services/inspection/info-car-control.service';
import { Brand } from './../../../Models/Abstract/Brand.interface';
import { CarClient } from './../../../Models/Abstract/CarClient.interface';

@Component({
    selector: 'cc-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InfoComponent implements OnInit {
    models: ModelCar[];
    @Input() brands: Brand[];
    @Input() inspectionCar: CarClient;
    @Input() selectedPageIndex: number;
    form: FormGroup;
    fields = Fields;

    constructor(
        private inspectionService: InspectionService,
        private infoCarControlService: InfoCarControlService,
    ) { }

    ngOnInit() {
        const fieldsCtrls =
            this.infoCarControlService.toFormGroup(
                this.inspectionService.inspectionParameters,
                'info',
                this.inspectionCar
            );

        const brandClient = this.inspectionService.getFieldValue('BRAND').chosenValue;
        const idMake = this.inspectionService.getBrandsContract(brandClient, this.brands)[0];
        this.inspectionService.getModel(idMake.id).subscribe(models => this.models = models);

        this.form = fieldsCtrls;
        this.inspectionService.saveFormInfo(fieldsCtrls);
        this.onChanges();
    }

    getModel(brand) {
        function filterItems(make, brands) {
            return brands.filter(el => {
                return el.name.toLowerCase().indexOf(make.toLowerCase()) > -1;
            });
        }

        const makeId = filterItems(brand, this.brands)[0];
        this.inspectionService.getModel(makeId.id).subscribe(
            (data) => {
                this.models = data;
            });
    }

    onChanges() {
        this.form.get('BRAND').valueChanges
            .subscribe(selectedBrand => {
                this.getModel(selectedBrand);
                const fieldModel = this.form.get('MODEL');
                fieldModel.setValue(null);
                fieldModel.markAsTouched();
            });
        const controlsForm: any = this.form.controls;

        this.form.valueChanges.subscribe(val => {
            Object.keys(val).forEach(field => {
                if (controlsForm[field]) {
                    if (controlsForm[field].controls) {
                        const { comment, chosenValue } = controlsForm[field].value;
                        this.inspectionService.saveInspectionInfo(field, chosenValue, comment);
                    } else {
                        const chosenValue = val[field];
                        this.inspectionService.saveInspectionInfo(field, chosenValue);
                    }
                }
            });
        });
    }

    nextTab() {
        if (this.form.valid) {
            // next tab
            this.inspectionService.changeNextTab(0);
        } else {
            this.validateAllFields(this.form);
        }
    }

    validateAllFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched();
            } else if (control instanceof FormGroup) {
                this.validateAllFields(control);
            }
        });
    }
}
