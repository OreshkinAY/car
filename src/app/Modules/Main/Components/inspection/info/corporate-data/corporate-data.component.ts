import { Component, OnInit, Input } from '@angular/core';
import { FormGroupDirective, ControlContainer } from '@angular/forms';

@Component({
  selector: 'cc-corporate-data',
  templateUrl: './corporate-data.component.html',
  styleUrls: ['./corporate-data.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective
    }
  ]
})
export class CorporateDataComponent implements OnInit {
  @Input() brands;
  @Input() models;

  constructor() { }

  ngOnInit() {
  }

}
