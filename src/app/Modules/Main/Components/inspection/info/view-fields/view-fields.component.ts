import { Component, OnInit, Input } from '@angular/core';
import { FormGroupDirective, ControlContainer } from '@angular/forms';

import { InspectionParamsId } from '../../../../Models/inspection-params-id';
import СommentsFieldsInspection from './../../../../Models/const/comments-fields-inspection';
import { InspectionService } from './../../../../Services/inspection/inspection.service';
import { InspectionParameters } from './../../../../Models/Abstract/InspectionParameters.interface';

@Component({
  selector: 'cc-view-fields',
  templateUrl: './view-fields.component.html',
  styleUrls: ['./view-fields.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective
    }
  ]
})
export class ViewFieldsComponent implements OnInit {
  @Input() fields;

  inspectionParamsId = InspectionParamsId;
  commentsField = СommentsFieldsInspection;
  inspectionParameters: InspectionParameters[];

  constructor(private inspectionService: InspectionService) {
    this.inspectionParameters = inspectionService.inspectionParameters;
  }

  ngOnInit() {
  }

  commentClass(field) {
    return this.commentsField[InspectionParamsId[field]];
  }

}
