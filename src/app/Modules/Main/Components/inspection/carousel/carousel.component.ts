import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { ImageSlider } from 'src/app/Modules/Main/Models/Abstract/ImageSlider.interface';

@Component({
  selector: 'cc-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  animations: [
    trigger('toggleState', [
      state('inactive', style({
        opacity: 0, transform: 'scale(0.5)', display: 'none'
      })),
      state('active', style({
        opacity: 1, transform: 'scale(1)'
      })),
      transition('inactive => active', animate('600ms cubic-bezier(0.47, 0, 0.745, 0.715)')),
      transition('active => inactive', animate('600ms cubic-bezier(0.47, 0, 0.745, 0.715)'))
    ])
  ],
})
export class CarouselComponent implements OnInit {
  counter: number;
  showList = false;
  @Input() images: ImageSlider[];
  @Input() idBigFile: number;
  @Output() closePanel = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.createImagesSlider(this.images, this.idBigFile);
  }

  createImagesSlider(images, idImg) {
    const position = images.map(e => e.id).indexOf(idImg);
    if (position > -1) {
      this.counter = position;
    }
  }

  closeCarousel() {
    this.closePanel.emit();
  }

  inc() {
    this.counter = (this.counter < this.total()) ? this.counter + 1 : 0;
  }

  dec() {
    this.counter = (this.counter > 0) ? this.counter - 1 : this.total();
  }

  total() {
    return this.images.length - 1;
  }

}
