import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material';

import inspectionsTabs from './../../../Shared/const/inspection-tabs';
import { InspectionService } from './../../Services/inspection/inspection.service';
import { CarClient } from './../../Models/Abstract/CarClient.interface';
import { Brand } from './../../Models/Abstract/Brand.interface';
import { DamagesComponent } from './../../Models/Abstract/DamagesComponent.interface';

@Component({
    selector: 'cc-inspection',
    templateUrl: './inspection.component.html',
    styleUrls: ['./inspection.component.scss']
})
export class InspectionComponent implements OnInit, OnDestroy {
    tabsLabel: { label: string, type: string }[];
    brands: Brand[];
    inspectionCar: CarClient;
    isLoaded = false;
    idCar: string;
    titlePanel: string;
    selectedPageIndex = 0;
    showRightPanel = false;
    component: DamagesComponent;
    nextActiveComponent: DamagesComponent;
    tabType: string;
    completeItem: any;
    activeTab = 0;

    constructor(
        private inspectionService: InspectionService,
        private routeActiv: ActivatedRoute
    ) {
        this.tabsLabel = inspectionsTabs;
        this.idCar = routeActiv.snapshot.url[1].path;
    }

    ngOnInit() {
        forkJoin(
            this.inspectionService.getInspectionCar(this.idCar),
            this.inspectionService.getInspectionParameters(),
            this.inspectionService.getBrands()
        ).subscribe((data) => {
            const car = data[0].car;
            this.titlePanel = car.make + ' ' + car.model;
            this.inspectionCar = car;
            this.inspectionService.inspectionParameters = data[1];
            this.brands = data[2];
            this.isLoaded = true;
        });

        this.inspectionService.selectedPageIndex$.subscribe(index => {
            this.selectedPageIndex = index;
        });
    }

    showComponent(data) {
        this.tabType = data.tabType;
        if (data.tabType === 'damage') {
            this.component = data.component;
            this.nextActiveComponent = data.component;
        } else {
            this.completeItem = data.component;
        }
        this.showRightPanel = true;
    }

    closeDamageComments() {
        this.showRightPanel = false;
    }

    activateDamageComments(position: DamagesComponent) {
        this.nextActiveComponent = position;
    }

    onLinkClick(event: MatTabChangeEvent) {
        this.selectedPageIndex = event.index;
        this.closeDamageComments();
    }

    ngOnDestroy() {
    }
}
