import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';

import { DamagesService } from './../../../../Services/inspection/damages.service';
import { DamagesComponent } from './../../../../Models/Abstract/DamagesComponent.interface';
import { InspectionService } from './../../../../Services/inspection/inspection.service';
import { InfoCarControlService } from '../../../../Services/inspection/info-car-control.service';
import Fields from './../../../../Models/const/fields';

@Component({
  selector: 'cc-damage-result',
  templateUrl: './damage-result.component.html',
  styleUrls: ['./damage-result.component.scss']
})
export class DamageResultComponent implements OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  components: DamagesComponent[] = [];
  fields = Fields;
  form: FormGroup;
  comment: string;
  countShowMiniatureimg = 3;

  constructor(
    private damagesService: DamagesService,
    private inspectionService: InspectionService,
    private infoCarControlService: InfoCarControlService,
  ) { }

  ngOnInit() {
    this.damagesService.components$.pipe(
      takeUntil(this.ngUnsubscribe)
    ).subscribe(
      (components) => {
        if (components.length) {
          this.components = components;
        }
      },
      (error) => console.log('error', error)
    );

    this.form = this.infoCarControlService.toFormGroup(
      this.inspectionService.inspectionParameters, 'result'
    );
    this.form.disable();

    this.damagesService.components$.subscribe(
      (components) => {
        if (components.length) {
          this.comment = components[components.length - 1].comment;
        }
      }
    );
  }
}
