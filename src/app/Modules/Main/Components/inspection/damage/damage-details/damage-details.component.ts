import { Component, OnInit, Input, Output, EventEmitter, OnChanges, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DamagesService } from './../../../../Services/inspection/damages.service';
import { DamagesComponent } from './../../../../Models/Abstract/DamagesComponent.interface';

@Component({
  selector: 'cc-damage-details',
  templateUrl: './damage-details.component.html',
  styleUrls: ['./damage-details.component.scss']
})
export class DamageDetailsComponent implements OnInit, OnChanges, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  components: DamagesComponent[] = [];
  @Input() nextActiveComponent: number;
  @Output() showDamageComments = new EventEmitter<any>(true);
  selectComments: number;
  nextComponent: any;

  constructor(private damagesService: DamagesService) { }

  ngOnInit() {
    this.damagesService.components$.pipe(
      takeUntil(this.ngUnsubscribe)
    ).subscribe(
      (components) => {
        if (components.length) {
          this.components = components;
        }
      },
      (error) => console.log('error', error)
    );
  }

  public ngOnChanges(nextActiveComponent) {
    this.nextComponent = nextActiveComponent.nextActiveComponent.currentValue;
    if (this.nextComponent !== undefined) {
      this.onSelectDamage(this.nextComponent);
    }
  }

  public onSelectDamage(component) {
    this.nextActiveComponent = +component.idComponent;
    this.showDamageComments.emit({
      'tabType': 'damage', component
    });
  }

  public isActiveComponent(component) {
    return this.nextActiveComponent === +component.idComponent;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
