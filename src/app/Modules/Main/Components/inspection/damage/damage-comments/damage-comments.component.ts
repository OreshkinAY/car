import { Component, OnInit, Input, Output, EventEmitter, OnChanges, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { DamagesService } from './../../../../Services/inspection/damages.service';
import { DamagesNameId } from '../../../../Models/const/damages-name';
import { DamagesComponent } from './../../../../Models/Abstract/DamagesComponent.interface';
import { Damage } from './../../../../Models/Abstract/Damage.interface';
import { PhotoVideoService } from 'src/app/Modules/Main/Services/inspection/photo-video.service';
import { ImageSlider } from 'src/app/Modules/Main/Models/Abstract/ImageSlider.interface';

@Component({
  selector: 'cc-damage-comments',
  templateUrl: './damage-comments.component.html',
  styleUrls: ['./damage-comments.component.scss']
})
export class DamageCommentsComponent implements OnInit, OnChanges, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  form: FormGroup;
  @Input() componentSelect: DamagesComponent;
  @Output() closeDamageComments = new EventEmitter();
  @Output() activeteComments = new EventEmitter<DamagesComponent>();
  damagesList: Damage[] = [];
  titleComments: string;
  damages: object;
  idImage: number;
  imageData: object;
  errorLoadImg: string;
  errorSaveImg: string;
  images: ImageSlider[] = [];
  idImageBig: number;
  showCarousel = false;
  showSlider = false;

  constructor(
    private damagesService: DamagesService,
    private photoVideoService: PhotoVideoService,
  ) {
  }

  ngOnInit() {
    this.damagesService.damages$.pipe(
      takeUntil(this.ngUnsubscribe)
    ).subscribe(
      (data) => {
        this.damagesList = data;
      }
    );
  }

  ngOnChanges(componentSelect) {
    const componentSelected = componentSelect.componentSelect.currentValue;
    const damages = componentSelected.damages;
    this.titleComments = componentSelected.name;
    this.damages = damages;
    const fieldsCtrls = {};

    for (const damage in damages) {
      if (damages.hasOwnProperty(damage)) {
        fieldsCtrls[damage] = new FormControl(damages[damage]);
      }
    }

    this.form = new FormGroup({
      damages: new FormGroup(fieldsCtrls, this.damagesValidator),
      comment: new FormControl(componentSelected.comment),
    });

    this.disableFieldComment(this.form.get('damages'));
    this.onChanges();
  }

  damagesValidator(control: FormControl): { [s: string]: boolean } {
    const valueDamages = Object.keys(control.value).map(key => control.value[key]);

    function isSelectOne(damage): boolean {
      return damage;
    }

    if (!valueDamages.some(isSelectOne)) {
      return { 'damages': true };
    }
    return null;
  }

  disableFieldComment(damages) {
    if (!damages.valid) {
      this.form.get('comment').reset();
      this.form.get('comment').disable();
      return;
    }
    this.form.get('comment').enable();
    return;
  }

  onChanges() {
    this.form.get('damages').valueChanges.subscribe(() => {
      this.disableFieldComment(this.form.get('damages'));
    });

    this.form.valueChanges.pipe(
      takeUntil(this.ngUnsubscribe)
    ).subscribe(selectDamage => {
      this.componentSelect.damages = selectDamage.damages;
      this.componentSelect.comment = selectDamage.comment;
    });
  }

  imageLoaded(data) {
    this.showSlider = true;
    this.componentSelect.photo.push(data.img);
  }

  removeMiniature(index) {
    this.componentSelect.photo.splice(index, 1);
  }

  showOriginalSize(idImg) {

    this.images = [];
    const tasksObservables: Observable<any> = this.photoVideoService.createDataCarousel(this.componentSelect.photo);

    forkJoin(tasksObservables).subscribe(
      () => {
        this.componentSelect.photo.forEach((process) => {
          this.images.push({
            'id': process.id,
            'bigFile': process.bigFile,
            'type': 'photo',
          });
          this.processHeader('0');
          document.body.style.overflow = 'hidden';
          this.idImageBig = idImg;
          this.showCarousel = true;
        });
      },
      (error) => this.errorLoadImg = error
    );
  }

  getControlName(damage: Damage): string {
    return DamagesNameId[damage.id];
  }

  damagesSelectComponent(damage) {
    return this.damages[DamagesNameId[damage.id]] !== undefined;
  }

  closePanel() {
    this.closeDamageComments.emit();
  }

  processHeader(value) {
    document.getElementById('header').style.zIndex = value;
  }

  closeCarousel() {
    this.processHeader('2');
    document.body.style.overflow = 'visible';
    this.showCarousel = false;
  }

  nextComponent() {
    this.damagesService.components$.pipe(
      takeUntil(this.ngUnsubscribe)
    ).subscribe(
      (components) => {
        if (components.length) {
          const position = components.map(e => {
            return e.idComponent;
          }).indexOf(this.componentSelect.idComponent);

          if (position > -1) {

            if (this.damagesService.validateCheckboxAndPhoto(components[position])) {
              this.componentSelect.validate = false;
              return;
            }

            const activePosition = components[position + 1];
            this.componentSelect.validate = true;
            this.activeteComments.emit(activePosition);
          }

        }
      },
      (error) => console.log('error', error)
    );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
