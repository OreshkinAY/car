import { Component, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'cc-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CommentComponent),
      multi: true
    }
  ]
})
export class CommentComponent implements ControlValueAccessor {

  value: string;
  onChange = (value: string) => { };
  onTouched = () => { };
  disabled: boolean;

  @ViewChild("input")
  input: ElementRef;

  constructor() { }

  writeValue(value: string): void {
    this.value = value ? value : '';
  }

  onBlur(value) {
    this.onChange(value);
    this.onTouched();
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.input.nativeElement.value = '';
    }
    this.disabled = isDisabled;
  }
}
