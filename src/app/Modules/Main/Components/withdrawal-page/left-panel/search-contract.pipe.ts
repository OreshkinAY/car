import { Pipe, PipeTransform } from '@angular/core';
import { Contracts } from './../../../Models/Abstract/ContractsData.interface';

@Pipe({
    name: 'searchContract'
})
export class SearchContractPipe implements PipeTransform {
    public transform(items: Contracts[], searchText: string, lenghtSearch?: boolean) {
        function searchContent(contract) {
            return contract.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
        }

        if (searchText) {
            const searchContract = items.filter((contract) => {
                return searchContent(contract.client.clientName) ||
                    searchContent(contract.car.vin) ||
                    searchContent(contract.creditAgreementNumber);
            });

            if (lenghtSearch) {
                return searchContract.length;
            }

            return searchContract;
        } else if (!searchText && lenghtSearch) {
            return items.length;
        }
        return items;
    }
}
