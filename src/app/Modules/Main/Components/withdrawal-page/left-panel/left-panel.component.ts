import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Contracts } from './../../../Models/Abstract/ContractsData.interface';

@Component({
  selector: 'cc-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent {
  nameTest: string;
  searchText: string;

  @Input() contracts: Contracts[];
  @Input() unActivate: boolean;
  @Output() takeContract = new EventEmitter<any>();

  selectContractId: any;

  constructor() { }

  selectContract(contract) {
    this.selectContractId = +contract.id;
    this.takeContract.emit(contract);
  }

  isActiveContract(contract) {
    if (this.unActivate) {
      return this.selectContractId === +contract.id;
    }
  }

}
