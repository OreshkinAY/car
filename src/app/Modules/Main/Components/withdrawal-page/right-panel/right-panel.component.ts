import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Contracts } from './../../../Models/Abstract/ContractsData.interface';

@Component({
  selector: 'cc-right-panel',
  templateUrl: './right-panel.component.html',
  styleUrls: ['./right-panel.component.scss']
})
export class RightPanelComponent {

  @Input() selectContract: Contracts;
  @Output() closePanel = new EventEmitter<any>();

  constructor(private router: Router) { }

  closeSummaryBlock() {
    this.closePanel.emit();
  }

  doInspection() {
    this.router.navigate(['/pages/inspection', this.selectContract.id]);
  }
}
