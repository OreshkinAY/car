import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ContractsService } from './../../Services/contracts.service';
import { Contracts } from './../../Models/Abstract/ContractsData.interface';

@Component({
  selector: 'cc-withdrawal-page',
  templateUrl: './withdrawal-page.component.html',
  styleUrls: ['./withdrawal-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WithdrawalPageComponent implements OnInit {
  contracts: Contracts[];
  selectContract: Contracts;
  activeContract: boolean;

  constructor(private contractsService: ContractsService) { }

  ngOnInit() {
    this.contractsService.getContracts().subscribe(
      (response) => {
        this.contracts = response;
        return this.contracts;
      }
    );
  }

  displayContractDetails(selectContract: Contracts) {
    this.selectContract = selectContract;
    this.activeContract = true;
  }

  closeRightPanel() {
    this.selectContract = null;
    this.activeContract = false;
  }

}
