import { Pipe, PipeTransform } from '@angular/core';

import { DamagePhoto } from './../Models/Abstract/DamagePhoto.interface';

@Pipe({
    name: 'damagesResultImages'
})
export class DamagesResultImages implements PipeTransform {
    transform(data: DamagePhoto[]) {
        return data.length - 3;
    }
}
