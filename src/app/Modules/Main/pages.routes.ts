import { Routes } from '@angular/router';
import { PagesRootComponent } from './Components/pages-root/pages-root.component';
import { AuthPageComponent } from './../Auth/Components/auth-page/auth-page.component';
import { ControlPageComponent } from './Components/control-page/control-page.component';
import { ComplaintsPageComponent } from './Components/complaints-page/complaints-page.component';
import { ReportsPageComponent } from './Components/reports-page/reports-page.component';
import { WithdrawalPageComponent } from './Components/withdrawal-page/withdrawal-page.component';
import { InspectionComponent } from './Components/inspection/inspection.component';

export const PagesRoutes: Routes = [
  {
    path: 'pages',
    component: PagesRootComponent,
    children: [
      {
        path: 'control',
        component: ControlPageComponent,
      },
      {
        path: 'complaints',
        component: ComplaintsPageComponent,
      },
      {
        path: 'reports',
        component: ReportsPageComponent,
      },
      {
        path: 'withdrawal',
        component: WithdrawalPageComponent,
      },
      {
        path: 'inspection/:id',
        component: InspectionComponent
      },
    ]
  },
  {
    path: 'auth',
    component: AuthPageComponent,
  }
];
