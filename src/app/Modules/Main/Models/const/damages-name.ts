export enum DamagesNameId {
    Deformation = 0, // Деформация
    Crack = 1, // Трещина
    Scratch = 2, // Царапина
    Сhipped = 3, // Скол
    Scor = 4, // Задир
    Corrosion = 5, // Коррозия
    Through_corrosion = 6, // Сквозная коррозия
    Peeling = 7, // Отслаивание
    Incision = 8, // Разрез
    Gap = 9, // Разрыв
    Flow = 10, //  Течь
    Destruction = 11, // Разрушение
    No_Damages = 12, // Отсутствие
}
