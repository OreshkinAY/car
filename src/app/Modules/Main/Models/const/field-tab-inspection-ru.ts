enum GeneralView {
    PHOTO_FRONT_LEFT = 'Спереди слева',
    PHOTO_FRONT_RIGHT = 'Спереди справа',
    PHOTO_REAR_LEFT = 'Сзади слева',
    PHOTO_REAR_RIGHT = 'Сзади справа',
    VIDEO_360 = 'Круговое видео'
}

enum DetailFoto {
    PHOTO_VIN = 'Фото VIN-кода',
    PHOTO_COMPARTMENT_FRONT = 'Фото передней части салона',
    PHOTO_ODOMETER = 'Фото показаний одометра при включенном зажигании (при наличии ключей от ТС)',
    PHOTO_TYRE = 'Фото шин и дисков',
    PHOTO_COMPARTMENT_REAR = 'Фото задней части салона',
    PHOTO_ENGINE_ROOM = 'Фото подкапотного пространства'
}

export { GeneralView, DetailFoto };
