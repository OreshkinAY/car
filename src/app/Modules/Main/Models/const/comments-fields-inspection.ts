import { InspectionParamsId } from '../inspection-params-id';

const СommentsFieldsInspection = {
    [InspectionParamsId.CARCASS_TYPE]: true,
    [InspectionParamsId.KEY_COUNT]: true,
    [InspectionParamsId.ALARMS_KEY_COUNT]: true,
    [InspectionParamsId.ALARM_EXISTENCE]: true,
    [InspectionParamsId.RESERVE_TIRE]: true,
    [InspectionParamsId.SEAT]: true,
    [InspectionParamsId.AUDIO_SYSTEM]: true,
    [InspectionParamsId.ALLOY_WHEELS]: true,
    [InspectionParamsId.WHEEL_COLLARS]: true,
};

export default СommentsFieldsInspection;
