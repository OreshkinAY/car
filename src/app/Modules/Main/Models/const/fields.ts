
const Fields = {
    'general-info': [
        'PTS',
        'CAR_MILLAGE',
        'CARCASS_TYPE',
        'PTS_СРТС'],
    'general-detail': [
        'ENGINE_TYPE',
        'ENGINE_POWER',
        'KPP_TYPE',
        'TYPE_OF_DRIVE',
        'AKB_EXISTENCE'],
    'signaling': [
        'KEYLESS_ACCESS',
        'KEY_COUNT',
        'ALARMS_KEY_COUNT',
        'ALARM_EXISTENCE'],
    'rubber': [
        'TIRES_TYPE',
        'REPAIR_SET',
        'RESERVE_TIRE'],
    'comfort-cabin': [
        'CLIMATE_CONTROL',
        'CONDITIONING',
        'ADDITIONAL_SEATS_ROW',
        'AUTOMATIC_SEAT_ADJ_PASS',
        'AUTOMATIC_SEAT_ADJ_DRV',
        'SEAT',
        'AUDIO_SYSTEM'], // 23
    'complectation': [
        'HEADLIGHT',
        'PARK_CONTROL',
        'CAMERA_VIEW_DEFAULT',
        'RUNNING_LIGHTS',
        'ALLOY_WHEELS',
        'WHEEL_COLLARS',
        'FOG_LIGHTS',
        'TOWING_DEVICE',
        'FRONT_BUMPER_GUARD',
        'REAR_BUMPER_GUARD',
        'RIGHT_STAGE',
        'LEFT_STAGE',
        'GAS_EQUIPMENT',
        'MAT_BAG',
        'LUKE',
        'DEAD_ZONES_CONTROL',
        'PANORAMIC_ROOF',
        'ROOF_RAILS',
        'MAT_CABIN', // 42
        'EMBLEMS',
        'ANTENNA',
    ],
    'additional-equipment': [
        'KPP_LOCK',
        'STEERING_LOCK',
        'ROOF_BOX',
        'MUD_FLAPS',
        'WINDOW_DEFLECTORS',
        'WHEEL_COLLARS',
        'HOOD_DEFLECTOR',
        'CRANKCASE_PROTECTION',
        'ADDITIONAL_FOG_LIGHTS',
        'KUNG',
        'PROTECTION_THRESHHOLDS',
        'PARKING_SYSTEM_REAR',
        'WINCH',
        'PRE_HEATER',
        'PARKING_SYSTEM_FRONT',
        'REINFORCED_BUMPER_REAR',
        'SPOILER',
        'REINFORCED_BUMPER_FRONT'
    ]
};

export default Fields;
