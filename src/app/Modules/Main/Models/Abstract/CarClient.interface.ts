interface ClientСontract {
    attachments: Attachments[];
    car: CarClient;
    client: Client;
    creditAgreementNumber: string;
    creditAgreementStatus: string;
    creditAgreementSubStatus: string;
    id: string;
    inspection: object;
    totalDebt: number;
    userFullName: string;
    userId: string;
}

interface CarClient {
    archival: boolean;
    color: string;
    enginePower: number;
    evaluationStatus: string;
    hasImportWarnings: boolean;
    id: string;
    licensePlate: string;
    make: string;
    manufactureYear: number;
    model: string;
    parkingId: string;
    pledgeAgreementNumber: string;
    vin: string;
}

interface Attachments {
    id: string;
    filename: string;
}

interface Client {
    clientAddress: string;
    clientId: string;
    clientName: string;
    clientPhoto: string;
}

export { CarClient, ClientСontract };
