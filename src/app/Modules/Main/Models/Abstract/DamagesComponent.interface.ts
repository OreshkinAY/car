import { DamagePhoto } from './../Abstract/DamagePhoto.interface';

export interface DamagesComponent {
    idComponent: string;
    name: string;
    field: string;
    damages: object;
    comment: string;
    photo: DamagePhoto[];
    validate: boolean;
}
