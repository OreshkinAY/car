export interface InspectionParameters {
    id: string;
    name: string;
    values: any[];
    type: string;
}
