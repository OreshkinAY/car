export interface DamagePhoto {
    id: number;
    miniature: any;
    bigFile: any;
}
