export interface ImageSlider {
    id: number;
    bigFile: any;
    type: string;
}
