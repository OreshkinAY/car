export interface Contracts {
    id: string;
    creditAgreementNumber: string;
    creditAgreementStatus: string;
    creditAgreementSubStatus: string;
    evaluationType: string;
    evaluationCreatedDate: string;
    totalDebt: number;
    userId: string;
    userFullName: string;
    car: Car;
    inspection: Inspection;
    client: Client;
    evaluation: Evaluation;
    attachments: AttachmentsItem[];
}
interface Car {
    id: string;
    pledgeAgreementNumber: string;
    make: string;
    model: string;
    manufactureYear: number;
    color: string;
    vin: string;
    licensePlate: string;
    enginePower: number;
    withdrawalDate: string;
    parkingId: string;
    archival: boolean;
    hasImportWarnings: boolean;
    evaluationStatus: string;
}
interface Inspection {
    id: string;
    carId: string;
    initialInspection: InitialInspectionItem[];
    generalPhotos: GeneralPhotosItem[];
    damagedComponents: DamagedComponentsItem[];
    followUpShortName: string;
    typeShortName: string;
    address: string;
    videoFileId: null;
    signatureFileId: null;
    pdfVersion: string;
    comment: string;
    datetime: string;
    isAbsent: boolean;
}
interface InitialInspectionItem {
    parameter: string;
    chosenValue?: string;
    comment: string;
}
interface GeneralPhotosItem {
    fileId: string;
    viewShortName: string;
}
interface DamagedComponentsItem {
    component: string;
    damages: string[];
    photos: string[];
    comment: string;
}
interface Client {
    clientId: string;
    clientName: string;
    clientAddress: string;
    clientPhoto: string;
}
interface Evaluation {
    carId: string;
    inspectionId: string;
    evaluationType: string;
    evaluationCompanyId: string;
    evaluationSum: null;
    city: string;
    attachmentId: null;
    creationComment: null;
    finishComment: null;
    createdDate: string;
    sendToEvaluation: string;
    returnedEvaluation: null;
    sendToEvaluationCompany: string;
    returnedFromEvaluationCompany: null;
    finishedEvaluation: null;
    clientFullName: string;
    proxyFullName: null;
    proxyPhone: string;
    proxyNumber: null;
    inspectionDateTime: null;
    inspectionAddress: string;
    lawyerFullName: string;
    lawyerPhone: string;
    lawyerProxyNumber: string;
}
interface AttachmentsItem {
    id: string;
    filename: string | null;
}
