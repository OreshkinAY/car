import { DamagesNameId } from './const/damages-name';
import { DamagePhoto } from './Abstract/DamagePhoto.interface';

class ComponentDamage {
    idComponent: string;
    name: string;
    field: string;
    damages: object;
    comment: string;
    photo: DamagePhoto[] = [];
    validate = false;
    constructor(id: string, field: string, name: string, damages?: string[]) {
        this.idComponent = id;
        this.field = field;
        this.name = name;
        this.damages = {};
        if (damages) {
            this.setDemages(damages);
        }
        this.comment = '';
    }

    public setDemages(damages) {
        damages.forEach(el => {
            this.damages[DamagesNameId[el]] = false;
        });
    }
}

export default ComponentDamage;
