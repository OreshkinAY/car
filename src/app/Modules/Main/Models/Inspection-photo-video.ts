export class InspectionPhotoVideo {
    id: number;
    miniature: any;
    bigFile: any;

    constructor(id: number, miniature: any) {
        this.id = id;
        this.miniature = miniature;
        this.bigFile = null;
    }
}
