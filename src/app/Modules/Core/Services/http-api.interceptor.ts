import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthService } from './../../Auth/Services/auth.service';
import { throwError } from 'rxjs';

@Injectable()
export class HttpApiInterceptor implements HttpInterceptor {
  // inflightAuthRequest = null;
  token: string;
  refreshToken: string;

  constructor(
    private injector: Injector,
    private authService: AuthService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // const authService = this.injector.get(AuthService);
    // const token = this.authService.getToken('token');

    // if (token) {
    //   request = request.clone({
    //     setHeaders: {
    //       Authorization: `Bearer ${token}`
    //     },
    //   });
    // }

    // if (!this.inflightAuthRequest) {
    const getToken = this.authService.getToken('token');
    if (!getToken) {
      return next.handle(request);
    }
    // this.inflightAuthRequest = token;
    // }
    // console.log('3333', this.inflightAuthRequest);


    return getToken.pipe(
      switchMap((token: string) => {
        // console.log('newToken', token);
        // this.token = token;

        // unset request inflight
        // this.inflightAuthRequest = null;

        // use the newly returned token
        const authReq = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`
          },
        });

        return next.handle(authReq);
      }), catchError(error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 401 && error.error.type === 'tokenExpiredException') {

            // if (!this.inflightAuthRequest) {
            //   // const token = this.authService.getToken('refreshToken');
            this.authService.getToken('refreshToken').subscribe(refreshToken => this.refreshToken = refreshToken);
            //   this.inflightAuthRequest = this.authService.refreshToken(this.refreshToken);
            // }
            // console.log('this.inflightAuthRequest', this.inflightAuthRequest);
            // console.log('this.refreshToken', this.refreshToken);

            return this.authService.refreshToken(this.refreshToken).pipe(
              switchMap((newToken) => {
                console.log('newToken222', newToken);
                console.log('newToken222--token', newToken.token);
                console.log('request--t!!', request);
                // unset inflight request
                // this.inflightAuthRequest = null;

                // clone the original request
                const authReqRepeat = request.clone({
                  setHeaders: {
                    Authorization: `Bearer ${newToken.token}`
                  },
                });
                // resend the request
                return next.handle(authReqRepeat);
              })
            );
          }
          return throwError(error);
        }
        return throwError(error);
      }));

    // return next.handle(request).pipe(catchError(error => {
    //     if (error instanceof HttpErrorResponse) {
    //       if (error.status === 401 && error.error.type === 'tokenExpiredException') {
    //         const refreshToken = this.authService.getToken('refreshToken');
    //         return this.authService.refreshToken(refreshToken).pipe(
    //           switchMap((newToken) => {
    //             request = request.clone({
    //               setHeaders: {
    //                 Authorization: `Bearer ${newToken.token}`
    //               }
    //             });
    //             return next.handle(request);
    //           })
    //         );
    //       }
    //       return throwError(error);
    //     }
    //     return throwError(error);
    //   }));
  }
}
