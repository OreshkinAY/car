import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpApiService {
  private URL = '/api/v1';
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = {headers: this.headers};

  constructor(private http: HttpClient) {
  }

  public post(path, data?): Observable<any> {
    return this.http.post(this.URL + path, data, this.options);
  }

  public put(path, data): Observable<any> {
    return this.http.put(this.URL + path, data);
  }

  public get(path): Observable<any> {
    return this.http.get(this.URL + path);
  }

  public getBlob(path): Observable<any> {
    return this.http.get(this.URL + path, { responseType: 'blob' });
  }
}
