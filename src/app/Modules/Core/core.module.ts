import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpApiService } from './../Core/Services/http-api.service';
import { StorageService } from './../Core/Services/storage.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpApiInterceptor } from './../Core/Services/http-api.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [
    HttpApiService,
    StorageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpApiInterceptor,
      multi: true,
    }
  ]
})
export class CoreModule { }
