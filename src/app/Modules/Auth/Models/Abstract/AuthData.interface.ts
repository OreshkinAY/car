export interface AuthDataRequest {
  login: string;
  password: string;
}
export interface AuthRefreshTokenRequest {
  refreshToken: string;
  token: string;
}
export interface AuthDataResponse {
  userId: number;
  token: string;
  refreshToken: string;
  role: string;
}
