import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpApiService } from './../Core/Services/http-api.service';
import { AuthService } from './Services/auth.service';
import { StorageService } from './../Core/Services/storage.service';
import { CoreModule } from './../Core/core.module';
import { SharedModule } from './../Shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreModule
  ],
  declarations: [],
  providers: [HttpApiService, AuthService, StorageService]
})
export class AuthModule { }
