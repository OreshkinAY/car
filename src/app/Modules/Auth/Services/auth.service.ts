import { Injectable } from '@angular/core';
import { HttpApiService } from './../../Core/Services/http-api.service';
import { AuthDataRequest, AuthDataResponse, AuthRefreshTokenRequest } from './../Models/Abstract/AuthData.interface';
import { Observable, throwError, of } from 'rxjs';
import { StorageService } from './../../Core/Services/storage.service';
import { map, catchError, share } from 'rxjs/operators';
import { log } from 'util';

@Injectable()
export class AuthService {
  private urlTokens = '/tokens';

  constructor(
    private httpApi: HttpApiService,
    private localStorage: StorageService) { }

  public doLogin(authData: AuthDataRequest): Observable<AuthDataResponse> {
    return this.httpApi.post(this.urlTokens, authData).pipe(
      map(response => {
        return this.writeTokenLocalStorage(response);
      }),
      catchError(err => {
        if (err.status === 401) {
          return throwError('Неверные логин/пароль');
        }
        return throwError(err.statusText);
      })
    );
  }

  public getToken(typeToken: string) {
    const currenToken = this.localStorage.localStorageRead('tokens');
    if (currenToken !== null) {
      // return currenToken[typeToken];
      return of(currenToken[typeToken]);
    }
    return null;
    // return this.refreshToken();
  }

  public refreshToken(refreshToken: string): Observable<any> {
    console.log('refreshToken', refreshToken);
    return this.httpApi.put(this.urlTokens, { refreshToken }).pipe(
      share(),
      map(response => {
        console.log('response', response);
        return this.writeTokenLocalStorage(response);
      })
    );
  }

  private writeTokenLocalStorage(response) {
    this.localStorage.localStorageWrite('tokens', response);
    console.log(response);

    return response;
  }

  public clearToken() {
    this.localStorage.localStorageRemove('tokens');
  }
}
