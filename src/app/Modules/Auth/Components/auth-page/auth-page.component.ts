import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../Services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'cc-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit {
  authForm: FormGroup;
  errorAuth: string;
  hide: boolean;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.authForm = new FormGroup({
      'login': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(3)])
    });
  }

  get login() { return this.authForm.get('login'); }
  get password() { return this.authForm.get('password'); }
  get passwordError() { return this.password['errors']; }

  isControlInvalidField(controlName: string): boolean {
    const control = this.authForm.controls[controlName];
    return control.invalid && control.touched;
  }

  checkFormInvalid(): boolean {
    const controlsForm = this.authForm.controls;

    if (this.authForm.invalid) {
      Object.keys(controlsForm).forEach(
        controlName => controlsForm[controlName].markAsTouched()
      );
      return true;
    }
    return false;
  }

  auth() {
    if (!this.checkFormInvalid()) {
      this.authService.doLogin({
        login: this.login.value,
        password: this.password.value
      }).subscribe(
        () => this.router.navigate(['/pages/withdrawal']),
        (error) => this.errorAuth = error
      );
    }
  }
}
